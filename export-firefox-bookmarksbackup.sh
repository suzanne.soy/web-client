#!/usr/bin/env nix-shell
#!nix-shell --pure -i bash -p dejsonlz4 jq bash -I nixpkgs=https://github.com/NixOS/nixpkgs/archive/fea633695211167753ee732c9087808d0c57461c.tar.gz

# Usage: ./export-firefox-bookmarks.sh [path/to/.mozilla/firefox/XXXXXXXX.profile]

set -euET -o pipefail

if test $# -ge 1; then
  profile="$1"
else
  # use printf to avoid echo's ambiguity; sort for determinism; use null separator (only byte illegal in UNIX paths, save trailing whitespace with a dummy x (not needed here as the string ends with "default", but a good habit nonetheless)
  profile="$(printf "%s"\\0 ~/.mozilla/firefox/*.default | sort -z | head -z -n 1 | tr -d \\0; printf "x")"
  profile="${profile%x}"
fi

  # use printf to avoid echo's ambiguity; sort to get latest backup; use null separator (only byte illegal in UNIX paths, save trailing whitespace with a dummy x (not neede here as the string ends with ".jsonlz4" but a good habit nonetheless)
js="$(printf "%s"\\0 "$profile"/bookmarkbackups/bookmarks-*.jsonlz4 | sort -z --general-numeric-sort | tail -z -n 1 | tr -d \\0; printf "x")"
js="${js%x}"

# TODO: filter out entries whose uri is null after checking that they're not important
# TODO: add folder structure to give a path for each bookmark

dejsonlz4 "$js" | jq 'recurse(.children?[]?) | "\( .title? ): \( .uri? )"'

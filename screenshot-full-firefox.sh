#!/usr/bin/env nix-shell
#!nix-shell --pure -i bash -p firefox-esr bash -I nixpkgs=https://github.com/NixOS/nixpkgs/archive/fea633695211167753ee732c9087808d0c57461c.tar.gz

set -euET -o pipefail

usage() {
  printf "Usage: ./screenshot-full-firefox.sh url filename"\\n
}

if test $# -ne 2; then
  usage
  exit 1
elif test $# -eq 1 && (test "x$1" = "x-h" || test "x$1" = "x--help"); then
  usage
  exit 0
fi

url="$1"
image="$2"

temp_profile_home="$(mktemp -d)"

function cleanup() {
  rm -fr --one-file-system "$temp_profile_home"
}

trap cleanup EXIT

HOME="$temp_profile_home" firefox --headless --window-size 1920 --screenshot "$image" "$url"

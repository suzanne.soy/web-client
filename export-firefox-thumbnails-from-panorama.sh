#!/bin/bash

set -euET -o pipefail

echo "This is just an example, best not to depend on the user having the panorama plug-in installed."
exit 1.

dejsonlz4 ~suzanne/apps/chromium/.mozilla/firefox/*.default/sessionstore-backups/recovery.jsonlz4 | jq '.windows[0].tabs[]|.extData["extension:panorama-tab-groups@example.com:thumbnail"]' -r | jq '.thumbnail'

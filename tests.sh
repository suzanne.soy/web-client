#!/usr/bin/env nix-shell
#!nix-shell -i bash -p bash nix -I nixpkgs=https://github.com/NixOS/nixpkgs/archive/fea633695211167753ee732c9087808d0c57461c.tar.gz

# Older nix-shell --pure versions prevent nested nix-shells to properly download dependencies, so the above doesn't use --pure.
# If this script increases in complexity and becomes more sensitive to env vars, use --pure and wrap it with an impure launcher.

set -euET -o pipefail

for i in tests/*.sh; do
  "$i"
done

#!/usr/bin/env nix-shell
#!nix-shell --pure -i bash -p bash nix -I nixpkgs=https://github.com/NixOS/nixpkgs/archive/fea633695211167753ee732c9087808d0c57461c.tar.gz

set -euET -o pipefail

cd "$(dirname "$0")"
../screenshot-full-firefox.sh https://example.com/ ../tests-results/screenshots-full-firefox.png

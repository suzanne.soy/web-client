#!/usr/bin/env nix-shell
#!nix-shell --pure -i bash -p dejsonlz4 jq bash -I nixpkgs=https://github.com/NixOS/nixpkgs/archive/fea633695211167753ee732c9087808d0c57461c.tar.gz

# Usage: ./export-firefox-bookmarks.sh [path/to/.mozilla/firefox/XXXXXXXX.profile]

set -euET -o pipefail

if test $# -ge 1; then
  profile="$1"
else
  # use printf to avoid echo's ambiguity; save trailing whitespace with a dummy x (not needed here as the string ends with "Default", but a good habit nevertheless)
  profile="$(printf "%sx" ~/.config/chromium/Default/)"
  profile="${profile%x}"
fi

jq '.roots[] | recurse(.children?[]?) | "\( .name? ): \( .url? )"' < "$profile"/Bookmarks
